# openwrt

# About
* [hub.docker.com](https://hub.docker.com/r/xdrum/openwrt/)
* [wikipedia.org](https://en.wikipedia.org/wiki/OpenWrt)

* [*Init Scripts*](https://openwrt.org/docs/techref/initscripts)
* [OpenRC](https://wiki.gentoo.org/wiki/OpenRC)
* [OpenRC](https://en.wikipedia.org/wiki/OpenRC)

* [Almquist shell](https://en.wikipedia.org/wiki/Almquist_shell)

* [Opkg](https://en.wikipedia.org/wiki/Opkg)
* [Opkg Package Manager](https://openwrt.org/docs/guide-user/additional-software/opkg)

* [package: procps-ng-ps](https://openwrt.org/packages/pkgdata/procps-ng-ps)

### Warning and error messages
* [Warning: requested NIC (anonymous, model unspecified) was not created (not supported by this machine?)](https://google.com/search?q=Warning%3A+requested+NIC+%28anonymous%2C+model+unspecified%29+was+not+created+%28not+supported+by+this+machine%3F%29)
  * Creating two NICs for OpenWrt on ARM

## Similar projects
* [travis-util/qemu](https://github.com/travis-util/qemu)@github
* [gitlab.com/qemu-demo/alpine-virt](https://gitlab.com/qemu-demo/alpine-virt)@gitlab

## Official documentation for basic configuration
* [guide-user:network:start](https://openwrt.org/docs/guide-user/network/start)
* [guide-quick-start:begin_here](https://openwrt.org/docs/guide-quick-start/begin_here)
* [guide-user:base-system:basic-networking](https://openwrt.org/docs/guide-user/base-system/basic-networking)
* [guide-user:virtualization:qemu OpenWrt in QEMU](https://openwrt.org/docs/guide-user/virtualization/qemu)

## Documentation
....